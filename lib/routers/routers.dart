import 'package:flutter/cupertino.dart';
import 'package:food_app/auths/logins/screens/login.dart';
import 'package:food_app/auths/passwords/changePasswords/change_password.dart';

import 'package:food_app/auths/registers/screens/register.dart';
import 'package:food_app/auths/welcomes/welcome.dart';
import 'package:food_app/components/bottom_navigation.dart';
import 'package:food_app/screens/accounts/screens/account.dart';
import 'package:food_app/screens/siders/sider.dart';

Route routeLogin() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => Login(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0, 1);
      var end = Offset.zero;
      var tween = Tween(begin: begin, end: end);
      var offsetAnimation = animation.drive(tween);
      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}
Route routeChangePassword() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => ChangePassword(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0, 1);
      var end = Offset.zero;
      var tween = Tween(begin: begin, end: end);
      var offsetAnimation = animation.drive(tween);
      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}

Route routeWelcome() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => Welcome(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(1, 0);
      var end = Offset.zero;
      var tween = Tween(begin: begin, end: end);
      var offsetAnimation = animation.drive(tween);
      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}

Route routeSlideWelcome() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => SliderWelcome(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(1, 0);
      var end = Offset.zero;
      var tween = Tween(begin: begin, end: end);
      var offsetAnimation = animation.drive(tween);
      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}

Route routeHome() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => BottomNavigator(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(1, 0);
      var end = Offset.zero;
      var tween = Tween(begin: begin, end: end);
      var offsetAnimation = animation.drive(tween);
      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}
Route routeAccount() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => Account(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(1, 0);
      var end = Offset.zero;
      var tween = Tween(begin: begin, end: end);
      var offsetAnimation = animation.drive(tween);
      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}

Route routeRegister() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => Register(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0, 1);
      var end = Offset.zero;
      var tween = Tween(begin: begin, end: end);
      var offsetAnimation = animation.drive(tween);
      return SlideTransition(
        position: offsetAnimation,
        child: child,
      );
    },
  );
}
// Route routeLogin() {
//   return PageRouteBuilder(
//       pageBuilder: (context, animation, secondaryAnimation) => Login());
// }
