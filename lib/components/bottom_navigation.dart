import 'package:flutter/material.dart';
import 'package:food_app/screens/accounts/screens/account.dart';
import 'package:food_app/screens/homes/screens/home.dart';
import 'package:food_app/screens/maps/screens/maps.dart';
import 'package:food_app/screens/products/screens/product.dart';

import '../constans.dart';

class BottomNavigator extends StatefulWidget {
  @override
  _BottomNavigatorState createState() => _BottomNavigatorState();
}

class _BottomNavigatorState extends State<BottomNavigator> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    HomePage(),
    Product(),
    MapPage(),
    Account(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          boxShadow: [BoxShadow(
            offset: Offset(0, 10),
            blurRadius: 50,
            color: kPrimaryColor.withOpacity(0.4)
          )]
        ),
        child: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Trang chủ',
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.favorite),
              label: 'Danh sách',
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.add_location_alt), label: 'Bản đổ'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Tài khoảng')
          ],
          currentIndex: _currentIndex, // this will be set when a new tab is tapped
          selectedItemColor: Colors.red,
          unselectedItemColor: kPrimaryColor,
          iconSize: 27,
          onTap: onTabTapped,
        ),
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

