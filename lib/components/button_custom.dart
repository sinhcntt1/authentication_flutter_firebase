import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constans.dart';

class ButtonCustom extends StatelessWidget {
  final Color color;
  final Color colorSide;
  final Function press;
  final String text;
  final Color colorText;


  const ButtonCustom(
      {Key key,
      this.color,
      this.colorSide,
      this.press,
      this.text, this.colorText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 13,
      width: size.width,
      margin: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2),
      child: FlatButton(
        onPressed: press,
        child: Text(
          text,
          style: TextStyle(color: colorText, fontSize: 20),
        ),
        color: color,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(29),
          side: BorderSide(color: colorSide), // mầu viền
        ),
      ),
    );
  }
}
