import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
class AuthService {
  final _auth = FirebaseAuth.instance;
//Create User in firebase
  //Đăng nhập với email và password
  Future<UserCredential> singUp(String username, String password) async {
    var resultUserSingUp = await _auth.createUserWithEmailAndPassword(
        email: username, password: password);
    return resultUserSingUp;
  }

//Login User in firebase
  Future<UserCredential> singIn(String username, String password) async {
    var resultUserSingIn = await _auth.signInWithEmailAndPassword(
        email: username, password: password);
    return resultUserSingIn;
  }

//Logout User in firebase
  Future<void> singOut() async {
    await _auth.signOut();
  }

  Future<void> forgotPassword(String username) async {
    await _auth.sendPasswordResetEmail(email: username);
  }

  Future<void> changePassword(String oldpassword, String newpassword) async {
    await _auth.confirmPasswordReset(code: oldpassword, newPassword: newpassword);
  }

  Future<User> getCurrentUser() async {
    var res = await _auth.currentUser;
    return res;
  }

//Server login đăng nhập với fasebook
  Stream<FirebaseUser> get currenUser => _auth.onAuthStateChanged;
  Future<UserCredential>  sinInWithCredentail(AuthCredential credential) => _auth.signInWithCredential(credential);
}



