import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/constans.dart';

class Logo extends StatelessWidget {
  final String title;

  const Logo({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var text = TextStyle(color: kPrimaryColor, fontSize: 36);
    return Column(
      children: [
        Align(
            alignment: Alignment.topLeft,
            child: IconButton(
                icon: Icon(
                  Icons.close,
                  color: kPrimaryColor,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                })),
        Container(
            child: Text(
          title,
          style: text,
        )),
      ],
    );
  }
}
