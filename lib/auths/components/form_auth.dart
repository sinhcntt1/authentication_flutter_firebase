import 'package:flutter/cupertino.dart';

import '../../constans.dart';

class FormAuth extends StatelessWidget {
  final Widget child;

  const FormAuth({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: kDefaultPadding * 4),
        color: kPrimaryColor,
        child: Container(
            decoration: BoxDecoration(
                color: kBackgroud,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(36),
                    topLeft: Radius.circular(36))),
            child: child),
      ),
    );
  }
}
