import 'package:flutter/cupertino.dart';

import '../../constans.dart';

class titleAccount extends StatelessWidget {
  final bool login;
  final Function onTap;

  const titleAccount({Key key, this.login = true, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        login
            ? Text('Bạn đả có tài khoảng chưa? ')
            : Text('Bạn đả có tài khoảng rồi? '),
        GestureDetector(
          onTap: onTap,
            child: Text(
          login ? 'Đăng ký.' : 'Đăng nhập.',
          style: TextStyle(color: kPrimaryColor),
        )),
      ],
    );
  }
}
