import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constans.dart';

class TextFieldCustoms extends StatelessWidget {
  final String labelText;
  final Icon prefixIcon;
  final Widget suffixIcon;
  final Function validator;
  final bool obscureText;
  final TextEditingController editingController;

  const TextFieldCustoms(
      {Key key,
      this.labelText,
      this.prefixIcon,
      this.suffixIcon,
      this.validator,
      this.obscureText = false,
      this.editingController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 13,
      margin: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2),
      child: TextFormField(
        validator: validator,
        obscureText: obscureText,
        controller: editingController,
        decoration: InputDecoration(
          fillColor: kPrimaryColor.withOpacity(0.1),
          filled: true,
          labelText: labelText,
          labelStyle: TextStyle(color: kPrimaryColor, fontSize: 20),
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(29),
          ),
          // tao mau vien cho form input
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(29), // bo tròn 4 góc
              borderSide: BorderSide(color: kPrimaryColor)),
        ),
      ),
    );
  }
}
