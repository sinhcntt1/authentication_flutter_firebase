import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/auths/registers/components/body_register.dart';

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BodyRegister(),
    );
  }
}