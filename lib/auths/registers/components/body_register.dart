

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/components/form_auth.dart';
import 'package:food_app/auths/components/logo.dart';
import 'package:food_app/auths/components/title_account.dart';
import 'package:food_app/auths/logins/components/text_field_password.dart';
import 'package:food_app/auths/registers/components/text_field_confirm_password.dart';
import 'package:food_app/auths/registers/components/text_field_username.dart';
import 'package:food_app/components/button_custom.dart';
import 'package:food_app/constans.dart';
import 'package:food_app/routers/routers.dart';
import 'package:food_app/services/auth_services.dart';


class BodyRegister extends StatefulWidget {
  @override
  _BodyRegisterState createState() => _BodyRegisterState();
}

class _BodyRegisterState extends State<BodyRegister> {
  bool ob = true;
  final authService = AuthService();
  TextEditingController username_editing = TextEditingController();

  TextEditingController password_editing = TextEditingController();

  TextEditingController confirmpassword_editing = TextEditingController();

  var _formKey = GlobalKey<FormState>();

  bool isLoading = false;



  void submmit() async {
    setState(() {

      isLoading = true;
    });
    try {
      // đăng kí người dùng
      await authService.singUp(username_editing.text, password_editing.text);
      // sau khi đăng khí người dùng xogn thì xác minh email
      //GetUser là currentUser(người dùng hiện tại
      //send mail để xác thực người dùng.
      await authService.getCurrentUser().then((value) {
        value.sendEmailVerification();
        Fluttertoast.showToast(msg: 'Vui lòng kiểm tra email để xác thực');

        //Sau khi xác thực xong chuyển về màng hình login để xác thực email
        Navigator.of(context).push(routeLogin());
        isLoading = false;
      }).catchError((onError) {
        Fluttertoast.showToast(
            msg: onError.toString(), toastLength: Toast.LENGTH_SHORT);
      });


    } on FirebaseAuthException catch(err) {
      //thực hiện show một số lỗi quan trọng khi người dùng đăng ký
      if(err.code == 'email-already-in-use'){
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: 'Email đăng lý đả tồn tại.',toastLength: Toast.LENGTH_SHORT);
      }

    } catch (e) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(
          msg: e.toString(), toastLength: Toast.LENGTH_SHORT);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FormAuth(
      child: Center(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Logo(
                title: 'ĐĂNG KÝ',
              ),
              SizedBox(
                height: 100,
              ),
              TextFieldUsername(
                controller: username_editing,
              ),
              SizedBox(
                height: 10,
              ),
              TextFieldPassword(
                controller: password_editing,
              ),
              SizedBox(
                height: 10,
              ),
              TextFieldConfirmPassword(
                controller: confirmpassword_editing,
                controller_confirmpassword: password_editing,
              ),
              SizedBox(
                height: 20,
              ),
              BtnRegister(context),
              SizedBox(
                height: 10,
              ),
              titleAccount(
                login: false,
                onTap: () {
                  Navigator.of(context).push(routeLogin());
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget BtnRegister(BuildContext context) {
    return isLoading != true
        ? ButtonCustom(
            color: kPrimaryColor,
            colorSide: kPrimaryColor,
            colorText: kPrimaryColorText,
            press: () {
              RegisterButton(context);
            },
            text: 'ĐĂNG KÝ',
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  void RegisterButton(BuildContext context) {
    if (_formKey.currentState.validate()) {
      submmit();
    }
  }
}
