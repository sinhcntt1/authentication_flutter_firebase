import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/components/text_field_customs.dart';

import '../../../constans.dart';

class TextFieldConfirmPassword extends StatefulWidget {
  final TextEditingController controller;
  final TextEditingController controller_confirmpassword;

  const TextFieldConfirmPassword({Key key, this.controller, this.controller_confirmpassword}) : super(key: key);
  @override
  _TextFieldConfirmPasswordState createState() => _TextFieldConfirmPasswordState();
}

class _TextFieldConfirmPasswordState extends State<TextFieldConfirmPassword> {
  bool ob = true;
  @override
  Widget build(BuildContext context) {
    return TextFieldCustoms(
      editingController: widget.controller,
      labelText: 'Xác nhận mật khẩu',
      prefixIcon: Icon(
        Icons.vpn_key,
        color: kPrimaryColor,
      ),
      obscureText: ob,
      suffixIcon: GestureDetector(
        onTap: () {
          setState(() {
            ob = !ob;
          });
        },
        child: Icon(
          ob == true ? Icons.visibility : Icons.visibility_off,
          color: kPrimaryColor,
        ),
      ),
      validator: (comfirmpassword) {
        if (comfirmpassword.toString().isEmpty) {
          Fluttertoast.showToast(
              msg: 'Vui lòng nhập xác nhận mật khẩu',
              toastLength: Toast.LENGTH_SHORT);
        }
        if (comfirmpassword != widget.controller_confirmpassword.text) {
          Fluttertoast.showToast(
              msg: 'Mật khẩu không khớp',
              toastLength: Toast.LENGTH_SHORT);
          throw ' mật khẩu không khớp';
        }
      },
    );
  }
}