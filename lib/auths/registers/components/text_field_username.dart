import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/components/text_field_customs.dart';

import '../../../constans.dart';

class TextFieldUsername extends StatelessWidget{
  final TextEditingController controller;

  const TextFieldUsername({Key key, this.controller}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextFieldCustoms(
      editingController: controller,
      labelText: 'Tài khoảng',
      prefixIcon: Icon(Icons.email, color: kPrimaryColor),
      validator: (username) {
        if (username.toString().isEmpty) {
          Fluttertoast.showToast(
              msg: 'Vui lòng nhập tài khoảng',
              toastLength: Toast.LENGTH_SHORT);
          throw 'tài khoảng trống';
        }
        bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            .hasMatch(controller.text);
        if (!emailValid) {
          Fluttertoast.showToast(
              msg: 'Định dạnh tài khoảng phải là Email',
              toastLength: Toast.LENGTH_SHORT);
          throw 'sai định dạng mail';
        }
      },
    );
  }
}