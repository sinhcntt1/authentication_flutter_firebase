import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/logins/components/text_field_password.dart';
import 'package:food_app/auths/logins/components/text_field_username.dart';
import 'package:food_app/auths/passwords/forgotPassword/screens/forgot_passsword.dart';
import 'package:food_app/components/button_custom.dart';
import 'package:food_app/services/auth_services.dart';
import 'btn_login.dart';
import 'package:food_app/auths/components/form_auth.dart';
import 'package:food_app/auths/components/logo.dart';
import 'package:food_app/auths/components/title_account.dart';
import 'package:food_app/constans.dart';
import 'package:food_app/routers/routers.dart';

class BodyLogin extends StatefulWidget {
  @override
  _BodyLoginState createState() => _BodyLoginState();
}

class _BodyLoginState extends State<BodyLogin> {
  final authService = AuthService();
  TextEditingController username_editing = TextEditingController();

  TextEditingController password_editing = TextEditingController();

  var _formKey = GlobalKey<FormState>();
  UserCredential result;
  bool isLoading = false;

  void sumbmit() async {
    setState(() {
      isLoading = true;
    });
    try {
      //Sau khi login thàng công thì app sẻ lấy các giá trị user hiên tại
      await authService.singIn(username_editing.text, password_editing.text);
      // sau khi login thành công thì sẻ push vào trang home
      Navigator.of(context).push(routeHome());
      setState(() {
        isLoading = false;
      });
    } on FirebaseAuthException catch (err) {
      // thực hiện hiển thị một số lổi quan trojg của firebase
      if (err.code == 'user-not-found') {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(
            msg: 'Email bạn vừa nhập không đúng',
            toastLength: Toast.LENGTH_SHORT);
      } else if (err.code == 'wrong-password') {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(
            msg: 'Mật khẩu ban vừa nhập không chính xác.',
            toastLength: Toast.LENGTH_SHORT);
      }

      // Còn các lổi của firebase
    } catch (err) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(
          msg: err.toString(), toastLength: Toast.LENGTH_SHORT);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FormAuth(
      child: Center(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Logo(
                title: 'ĐĂNG NHẬP',
              ),
              SizedBox(
                height: 30,
              ),
              ButtonLoginWithGoogle(
                press: () {},
              ),
              SizedBox(
                height: 10,
              ),
              ButtonLoginWithFacebook(
                press: () {
                  haldelLoginWithFacebook(context);
                },
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                child: Text('Hoặc đăng nhập bằng tài khoảng của bạn'),
              ),
              SizedBox(
                height: 20,
              ),
              TextFiieldUsername(
                controller: username_editing,
              ),
              SizedBox(
                height: 10,
              ),
              TextFieldPassword(
                controller: password_editing,
              ),
              SizedBox(
                height: 20,
              ),
              isLoading == false
                  ? ButtonLogin(context)
                  : Center(
                      child: CircularProgressIndicator(),
                    ),
              SizedBox(
                height: 10,
              ),
              GestureDetector(
                onTap: () {
                  return showDialog(
                      context: context,
                      builder: (BuildContext context) =>
                          ForgotPasswordDiaLog());
                },
                child: Text('Quên mật khẩu',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    )),
              ),
              SizedBox(
                height: 10,
              ),
              titleAccount(
                onTap: () {
                  Navigator.of(context).push(routeRegister());
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  var fb = FacebookLogin();

  Future<void> haldelLoginWithFacebook(BuildContext context) async {
    final res = await fb.logIn(permissions: [
      FacebookPermission.publicProfile,
      FacebookPermission.email
    ]);

    //Kiểm tra có lổi gì với bản điều khiển khi đằng nhập với facebook hay không
    switch (res.status) {
      case FacebookLoginStatus.success:
        setState(() {
          isLoading = true;
        });

        try{
          // lấy token. Mã truy cập của người dùng trang web.
          final FacebookAccessToken fbToken = res.accessToken;
          // vovert to Au th credenal

          final AuthCredential credential = FacebookAuthProvider.credential(fbToken.token);

          //User Credential to Sign in With Firebase
          var userCurent =  await authService.sinInWithCredentail(credential);

          //Kiểm tra User đả đăng nhập hay chưa
          if(userCurent !=null){
            Navigator.of(context).push(routeHome());
            Fluttertoast.showToast(msg: 'Đăng nhập thành công.',toastLength: Toast.LENGTH_SHORT);
            setState(() {
              isLoading = false;
            });
          } else {
            Fluttertoast.showToast(msg: 'Vui lòng đăng nhập');
            setState(() {
              isLoading = false;
            });
          }

        } catch(err){
          Fluttertoast.showToast(msg: err.toString(), toastLength: Toast.LENGTH_SHORT);
        }
        break;
      case FacebookLoginStatus.cancel:
        print('userCancal');
        break;
      case FacebookLoginStatus.error:
        print('userError');
        break;
    }
  }

  ButtonCustom ButtonLogin(BuildContext context) {
    return ButtonCustom(
      press: () {
        if (_formKey.currentState.validate()) {
          sumbmit();
        }
      },
      text: 'Đăng nhập',
      colorText: Colors.white,
      color: kPrimaryColor,
      colorSide: kPrimaryColor,
    );
  }
}
