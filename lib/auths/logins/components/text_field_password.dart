import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/components/text_field_customs.dart';

import '../../../constans.dart';

class TextFieldPassword extends StatefulWidget {
  final TextEditingController controller;

  const TextFieldPassword({Key key, this.controller}) : super(key: key);

  @override
  _TextFieldPasswordState createState() => _TextFieldPasswordState();
}

class _TextFieldPasswordState extends State<TextFieldPassword> {
  bool ob = true;
  @override
  Widget build(BuildContext context) {
    return TextFieldCustoms(
      editingController: widget.controller,
      obscureText: ob,
      labelText: 'Mật khẩu',
      prefixIcon: Icon(Icons.vpn_key, color: kPrimaryColor),
      suffixIcon: GestureDetector(
        onTap: (){
          setState(() {
            ob = !ob;
          });
        },
        child: Icon(
          ob == true ? Icons.visibility : Icons.visibility_off,
          color: kPrimaryColor,
        ),
      ),
      validator: (password) {
        if (password.isEmpty) {
          Fluttertoast.showToast(
              msg: 'Vui lòng nhập mật khẩu', toastLength: Toast.LENGTH_SHORT);
          throw 'vui long nhap mật khẩu';
        }
        if (password.toString().length < 6) {
          Fluttertoast.showToast(
              msg: 'Mật khẩu phải dài hơn 6 kí tự',
              toastLength: Toast.LENGTH_SHORT);
          throw 'mật khẩu phải dài hơn 6 kí tự';
        }
      },
    );
  }
}