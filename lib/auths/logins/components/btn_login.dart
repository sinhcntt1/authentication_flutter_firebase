import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/components/button_custom.dart';
import 'package:food_app/routers/routers.dart';

import '../../../constans.dart';

class ButtonLoginWithFacebook extends StatelessWidget {
  final Function press;

  const ButtonLoginWithFacebook({Key key, this.press}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ButtonCustom(
      press: press,
      text: 'Đăng nhập với số Facebook',
      colorText: Colors.white,
      color: Colors.blue,
      colorSide: Colors.blue,
    );
  }
}
class ButtonLoginWithGoogle extends StatelessWidget {
  final Function press;

  const ButtonLoginWithGoogle({Key key, this.press}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ButtonCustom(
      press: press,
      text: 'Đăng nhập với Google',
      colorText: Colors.white,
      color: Colors.red,
      colorSide: Colors.red,
    );
  }
}


