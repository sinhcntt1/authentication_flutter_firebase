import 'package:flutter/material.dart';
import 'package:food_app/constans.dart';
import 'package:food_app/routers/routers.dart';

import '../../../components/button_custom.dart';

class BodyWelcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: kPrimaryColor,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(300),
            topLeft: Radius.circular(300),
          ),
          color: kBackgroud,
        ),
        child: Padding(
          padding: EdgeInsets.only(bottom: kDefaultPadding * 5),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Image.asset('assets/images/logo.png'),
                ),
                ButtonCustom(
                  press: () {
                    Navigator.of(context).push(routeLogin());
                  },
                  color: kPrimaryColor,
                  text:'Đăng nhập',
                  colorText: Colors.white,
                  colorSide: kPrimaryColor,
                ),
                SizedBox(
                  height: 10,
                ),
                ButtonCustom(
                  press: () {
                    Navigator.of(context).push(routeRegister());
                  },
                  text: 'Đăng ký',
                  colorText: kPrimaryColor,
                  colorSide: kPrimaryColor,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
