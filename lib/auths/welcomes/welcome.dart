import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'components/body.dart';

class Welcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BodyWelcome(),
    );
  }

  AppBar AppBarBuil() {
    return AppBar(
      title: Center(
          child: Text(
        'WelCome',
        style: TextStyle(fontSize: 25),
      )),
    );
  }
}
