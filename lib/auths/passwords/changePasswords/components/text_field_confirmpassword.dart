import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/components/text_field_customs.dart';

import '../../../../constans.dart';

class TextFieldConfirmPasssword extends StatefulWidget{
  final TextEditingController controller;
  final TextEditingController confirmController;

  const TextFieldConfirmPasssword({Key key, this.controller, this.confirmController}) : super(key: key);
  @override
  _TextFieldConfirmPassswordState createState() => _TextFieldConfirmPassswordState();
}

class _TextFieldConfirmPassswordState extends State<TextFieldConfirmPasssword> {
  bool ob = true;
  @override
  Widget build(BuildContext context) {
    return TextFieldCustoms(
        editingController: widget.controller,
        validator: (cofirmpassword) {
          if(cofirmpassword.toString().isEmpty){
            Fluttertoast.showToast(msg: 'Vui lòng xác nhận mật khẩu.', toastLength: Toast.LENGTH_SHORT);
            throw 'nhận xác nhận mật khẩu trống.';
          }
          if(cofirmpassword.toString() != widget.confirmController.text){
            Fluttertoast.showToast(msg: 'Mật khẩu không khớp.', toastLength: Toast.LENGTH_SHORT);
            throw 'mật khẩu không khớp.';
          }
        },
        labelText: 'Xác nhận mật khẩu',
        prefixIcon: Icon(
          Icons.vpn_key,
          color: kPrimaryColor,
        ),
        obscureText: ob,
        suffixIcon: GestureDetector(
          onTap: (){
            setState(() {
              ob = !ob;
            });
          },
          child: Icon(ob == true ? Icons.visibility : Icons.visibility_off, color: kPrimaryColor,),
        )
    );
  }
}