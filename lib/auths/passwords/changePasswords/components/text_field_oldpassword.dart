import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/components/text_field_customs.dart';

import '../../../../constans.dart';

class TextFieldOldPasssword extends StatefulWidget {
  final TextEditingController controller;

  const TextFieldOldPasssword({Key key, this.controller}) : super(key: key);
  @override
  _TextFieldOldPassswordState createState() => _TextFieldOldPassswordState();
}

class _TextFieldOldPassswordState extends State<TextFieldOldPasssword> {
  bool ob= true;
  @override
  Widget build(BuildContext context) {
    return TextFieldCustoms(
        editingController: widget.controller,
        validator: (oldPassword) {
          if(oldPassword.toString().isEmpty){
            Fluttertoast.showToast(msg: 'Vui lòng nhập mật khẩu củ.', toastLength: Toast.LENGTH_SHORT);
            throw 'mật khẩu trống';
          }
          if(oldPassword.toString().length < 6){
            Fluttertoast.showToast(msg: 'Mật khẩu củ phải dài hơn 6.', toastLength: Toast.LENGTH_SHORT);
            throw 'mật khẩu trống';
          }
        },
        labelText: 'Mật khẩu củ',
        prefixIcon: Icon(
          Icons.vpn_key,
          color: kPrimaryColor,
        ),
        obscureText: ob,
        suffixIcon: GestureDetector(
          onTap: (){
            setState(() {
              ob =! ob;
            });
          },
          child: Icon(ob == true ? Icons.visibility : Icons.visibility_off, color: kPrimaryColor,),
        )
    );
  }
}