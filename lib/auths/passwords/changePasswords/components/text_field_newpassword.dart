import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/components/text_field_customs.dart';

import '../../../../constans.dart';

class TextFieldNewPassword extends StatefulWidget {
  final TextEditingController controller;

  const TextFieldNewPassword({Key key, this.controller}) : super(key: key);
  @override
  _TextFieldNewPasswordState createState() => _TextFieldNewPasswordState();
}

class _TextFieldNewPasswordState extends State<TextFieldNewPassword> {
  bool ob = true;
  @override
  Widget build(BuildContext context) {
    return TextFieldCustoms(
        editingController: widget.controller,
        obscureText: ob,
        validator: (newpassword) {
          if(newpassword.toString().isEmpty){
            Fluttertoast.showToast(msg: 'Vui lòng nhập mật khẩu mới.', toastLength: Toast.LENGTH_SHORT);
            throw 'mật khẩu trống';
          }
          if(newpassword.toString().length < 6){
            Fluttertoast.showToast(msg: 'Mật khẩu mới phải dài hơn 6.', toastLength: Toast.LENGTH_SHORT);
            throw 'mật khẩu phải dài hơn 6 kí tự';
          }
        },
        labelText: 'Mật khẩu mới',
        prefixIcon: Icon(
          Icons.vpn_key,
          color: kPrimaryColor,
        ),
        suffixIcon: GestureDetector(
          onTap: (){
            setState(() {
              ob =! ob;
            });
          },
          child: Icon(ob == true ? Icons.visibility : Icons.visibility_off, color: kPrimaryColor,),
        )
    );
  }
}