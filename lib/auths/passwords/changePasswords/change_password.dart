import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/auths/components/form_auth.dart';
import 'package:food_app/auths/components/logo.dart';
import 'package:food_app/components/button_custom.dart';
import 'package:food_app/constans.dart';
import 'package:food_app/routers/routers.dart';
import 'package:food_app/services/auth_services.dart';
import 'components/text_field_confirmpassword.dart';
import 'components/text_field_newpassword.dart';
import 'components/text_field_oldpassword.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final authService = AuthService();
  TextEditingController oldpassword_editing = TextEditingController();

  TextEditingController newpassword_editing = TextEditingController();

  TextEditingController confirm_editing = TextEditingController();

  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FormAuth(
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Logo(
              title: 'ĐỔI MẬT KHẨU',
            ),
            SizedBox(
              height: 100,
            ),
            TextFieldOldPasssword(
              controller: oldpassword_editing,
            ),
            SizedBox(
              height: 10,
            ),
            TextFieldNewPassword(
              controller: newpassword_editing,
            ),
            SizedBox(
              height: 10,
            ),
            TextFieldConfirmPasssword(
              controller: confirm_editing,
              confirmController: newpassword_editing,
            ),
            SizedBox(
              height: 20,
            ),
            isLoading == false
                ? ButtonRegister(context)
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ],
        ),
      ),
    ));
  }

  ButtonCustom ButtonRegister(BuildContext context) {
    return ButtonCustom(
      color: kPrimaryColor,
      colorSide: kPrimaryColor,
      text: 'Đổi mật khẩu',
      colorText: kPrimaryColorText,
      press: () {
        if (_formKey.currentState.validate()) {
          ButtonChangePassword(context);
        }
      },
    );
  }

  bool isLoading = false;

  Future<void> ButtonChangePassword(BuildContext context) async {
    setState(() {
      isLoading = true;
    });

    // lấy ra user hiện tại
    var user = await authService.getCurrentUser();

    // ignore: unrelated_type_equality_checks
    if(oldpassword_editing.text == user){
      print('ok');
    } else{
      print('k ok');
    }

    //kiểm tra xem user hiện tại đả có chưa. Nếu chưa có thì bắt buộc login
    if (user != null) {
      try {
        // lấy mật khẩu của user hiện tại đả đăng nhập.
        // tiếng hành update password mới.
        //paramerter là giá trị nhập  vào của textfild new_password
        // nêu điều kiện ok thì sẻ tiến hành đổi password và logout bắt người dùng phải đăng nhập lại.
        await FirebaseAuth.instance.currentUser
            .updatePassword(newpassword_editing.text)
            .then((value) async {
          setState(() {
            isLoading = false;
          });
          await authService.singOut();
          Navigator.of(context).push(routeLogin());
          Fluttertoast.showToast(
              msg: 'Đổi mật khẩu thành công.', toastLength: Toast.LENGTH_SHORT);
        })
            // Neeys đổi không thành công thì sẻ log lổi ra
            // firebase sẻ hổ trợ việc log lổi ra cho chúng ta.
            .catchError((onError) {
              setState(() {
                isLoading = false;
              });

              Fluttertoast.showToast(msg: onError.toString(),toastLength: Toast.LENGTH_SHORT);
        });

        //Try catch
      } catch (err) {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: err.toString(),toastLength: Toast.LENGTH_SHORT);
      }
    }
    //chưa login thì sẻ chuyển qua trang login
    else {
      Navigator.of(context).push(routeLogin());
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: 'Bạn phải đăng nhập mới đổi mật khẩu được.',toastLength: Toast.LENGTH_SHORT);
    }

  }
}
