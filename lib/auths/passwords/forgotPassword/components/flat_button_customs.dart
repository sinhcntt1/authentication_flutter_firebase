
import 'package:flutter/material.dart';

class FlatButtonCustoms extends StatelessWidget {
  final Color color;
  final Color colorSide;
  final String title;
  final Color titleColor;
  final Function press;

  const FlatButtonCustoms({Key key, this.color, this.title, this.titleColor, this.colorSide, this.press})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width / 4,
      child: FlatButton(

          color: color,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: colorSide)),
          onPressed: press,
          child: Text(
            title,
            style: TextStyle(fontSize: 20, color: titleColor),
          )),
    );
  }
}
