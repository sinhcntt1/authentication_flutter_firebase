import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../constans.dart';


class TextFieldForgot  extends StatelessWidget {
  final Function validator;
  final TextEditingController controller;

  const TextFieldForgot({Key key, this.validator, this.controller}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 16,
      margin: EdgeInsets.symmetric(horizontal: kDefaultPadding),
      child: TextFormField(
        controller: controller,
        validator: validator,
        decoration: InputDecoration(
          labelText: 'Tài khoảng',
          labelStyle: TextStyle(color: kPrimaryColor, fontSize: 20),
          prefixIcon: Icon(Icons.email, color: kPrimaryColor, size: 28,),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: kPrimaryColor)),
        ),
      ),
    );
  }
}