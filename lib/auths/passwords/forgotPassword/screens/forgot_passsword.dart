import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/constans.dart';
import 'package:food_app/services/auth_services.dart';


import '../components/flat_button_customs.dart';
import '../components/textfield_forgot.dart';

class ForgotPasswordDiaLog extends StatefulWidget {
  @override
  _ForgotPasswordDiaLogState createState() => _ForgotPasswordDiaLogState();
}

class _ForgotPasswordDiaLogState extends State<ForgotPasswordDiaLog> {
  final authService = AuthService();
  TextEditingController username_editing = TextEditingController();

  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Dialog(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            height: size.height / 4,
            width: size.width / 1.2,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(36),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildTextFieldForgot(),
                isLoading == false ? buildBtn(context) : Padding(
                  padding: EdgeInsets.only(top: kDefaultPadding /2),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  TextFieldForgot buildTextFieldForgot() {
    return TextFieldForgot(
      controller: username_editing,
      validator: (username) {
        if (username.toString().isEmpty) {
          Fluttertoast.showToast(
              msg: 'Vui lòng nhập tài khoảng', toastLength: Toast.LENGTH_SHORT);
          throw 'user name isEmpty';
        }
        bool emailValid = RegExp(
                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            .hasMatch(username_editing.text);
        if (!emailValid) {
          print('a: ${username_editing.text}');
          Fluttertoast.showToast(
              msg: 'Định dạnh tài khoảng phải là Email',
              toastLength: Toast.LENGTH_SHORT);
          throw 'sai dinh dang mail';
        }
      },
    );
  }

  Container buildBtn(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: kDefaultPadding),
      child: ButtonBar(
        children: [
          FlatButtonCustoms(
            press: () {
              Navigator.of(context).pop();
            },
            title: 'Thoát',
            color: kPrimaryColorText,
            titleColor: kPrimaryColor,
            colorSide: kPrimaryColor,
          ),
          FlatButtonCustoms(
            press: () {
              if (_formKey.currentState.validate()) {
                forgotPasswordBtn();
              }
            },
            title: 'Gửi',
            color: kPrimaryColor,
            titleColor: kPrimaryColorText,
            colorSide: kPrimaryColor,
          ),
        ],
      ),
    );
  }

  bool isLoading = false;

  Future<void> forgotPasswordBtn() async {
    setState(() {
      isLoading = true;
    });
    try {
      await authService.forgotPassword(username_editing.text);
      Fluttertoast.showToast(
          msg: 'Vui lòng kiểm tra email', toastLength: Toast.LENGTH_SHORT);
      Navigator.of(context).pop();
      setState(() {
        isLoading = false;
      });
    } catch (err) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(
          msg: err.toString(), toastLength: Toast.LENGTH_SHORT);
      throw err.toString();
    }
  }
}
