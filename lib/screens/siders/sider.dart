import 'package:carousel_pro/carousel_pro.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/components/button_custom.dart';
import 'package:food_app/constans.dart';
import 'package:food_app/routers/routers.dart';
import 'package:food_app/services/auth_services.dart';


class SliderWelcome extends StatelessWidget {
  final authService = AuthService();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: [
        Container(
            height: size.height,
            width: size.width,
            child: Carousel(
              images: [
                AssetImage('assets/images/image_sider1.jpg',),
                AssetImage('assets/images/image_sider3.jpg'),
                AssetImage('assets/images/image_slider2.jpg'),
              ],
              boxFit: BoxFit.fill,
              dotSize: 7.0,
              dotSpacing: 25.0,
              dotIncreaseSize: 1.2,
              dotIncreasedColor: Colors.white,
              dotColor: kPrimaryColor.withOpacity(0.38),
              indicatorBgPadding: 100,
              dotBgColor: kPrimaryColor.withOpacity(0),
              autoplay: true,
              autoplayDuration: Duration(seconds: 5),
              moveIndicatorFromBottom: 180.0,
              noRadiusForIndicator: true,
            )),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 350,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2),
              child: ButtonCustom(
                color: kPrimaryColor.withOpacity(0.5),
                colorSide: kPrimaryColor,
                colorText: kPrimaryColorText,
                text: 'BẮT ĐẦU',
                press: () {
                  sumbmit(context);
                },
              ),
            ),
          ],
        )
      ],
    ));
  }

  void sumbmit(BuildContext context) async{
    // ignore: deprecated_member_use
    var user = await authService.getCurrentUser();
    // kiểm tra đăng nhập nếu người dùng chưa đăng nhập thì user sẻ null
    // Nếu đả đăng nhập rồi thì btn sẻ chuyền và trực tiếp home
    // còn không thì bắn người dùng login
    if(user != null){
      Navigator.of(context).push(routeHome());
    } else {
      Navigator.of(context).push(routeWelcome());
    }
  }
}
