import 'package:flutter/cupertino.dart';
import 'package:food_app/constans.dart';

class BodyHomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          height: size.height/5,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60), bottomRight: Radius.circular(60)),
            color: kPrimaryColor
          ),
        )
      ],
    );
  }
}