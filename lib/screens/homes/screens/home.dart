import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/constans.dart';
import 'package:food_app/screens/homes/components/body_home_page.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Row(
        children: [
          Text('Home', style: TextStyle(color: kPrimaryColorText),),
          Spacer(),
          IconButton(icon: Icon(Icons.search, color : kPrimaryColorText), onPressed: null)
        ],
      ),automaticallyImplyLeading: false),

      body: BodyHomePage(),
    );
  }
}