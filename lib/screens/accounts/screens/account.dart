import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/screens/accounts/components/body_account.dart';


import '../../../constans.dart';

class Account extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              'Tài khoảng',
              style: TextStyle(color: kPrimaryColorText),
            ),
          ),
            automaticallyImplyLeading: false
        ),
        body: BodyAccount());
  }
}
