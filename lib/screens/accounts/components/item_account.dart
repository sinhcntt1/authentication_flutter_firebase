import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../constans.dart';

class ItemAccount extends StatelessWidget {
  final Function ontap;
  final Icon icon;
  final String title;

  const ItemAccount({Key key, this.ontap, this.icon, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(bottom: kDefaultPadding / 2),
        height: size.height / 15,
        decoration: BoxDecoration(
            color: kPrimaryColorText,
            border: Border.all(color: kPrimaryColor),

        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: Row(
            children: [
              Text(title, style: TextStyle(fontSize: 20),),
              Spacer(),
              icon
            ],
          ),
        ),
      ),
      onTap: ontap
    );
  }
}