import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/services/auth_services.dart';

import '../../../constans.dart';

class AvatartBacgroud extends StatefulWidget {


  @override
  _AvatartBacgroudState createState() => _AvatartBacgroudState();
}

class _AvatartBacgroudState extends State<AvatartBacgroud> {
  var authService = AuthService();
  User _user;
  void initState()  {
    super.initState();
    getUser();
  }

  Future<void> getUser () async{
    var user = await authService.getCurrentUser();
    setState(() {
      _user = user;
    });
  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          height: size.height / 2.7,
          child: Stack(
            children: [
              Container(
                height: size.height / 3 - 27,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        alignment: Alignment.topLeft,
                        fit: BoxFit.cover,
                        image: AssetImage('assets/images/backgroud1.jpg'))),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: CircleAvatar(
                  radius: 70,
                  backgroundImage:  _user==null ? AssetImage('assets/images/avatart.jpg') : NetworkImage('${_user.photoURL}'),
                  backgroundColor: kPrimaryColor,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: kDefaultPadding / 2, bottom: kDefaultPadding),
          child: Text(_user == null ? 'Null' : '${_user.displayName}', style: TextStyle(fontSize: 27),),
        )
      ],
    );
  }
}
