import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food_app/components/button_custom.dart';
import 'package:food_app/routers/routers.dart';
import 'package:food_app/services/auth_services.dart';

import '../../../constans.dart';
import 'avartat_backgroud.dart';
import 'item_account.dart';

class BodyAccount extends StatefulWidget {
  @override
  _BodyAccountState createState() => _BodyAccountState();
}

class _BodyAccountState extends State<BodyAccount> {
  final authService = AuthService();
  User _user;

  void initState() {
    super.initState();
    getUser();
  }

  Future<void> getUser() async {
    var user = await authService.getCurrentUser();
    setState(() {
      _user = user;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: _user == null ? buildCenter(context) : bodyAccount(context),
    );
  }

  Column bodyAccount(BuildContext context) {
    return Column(
      children: [
        AvatartBacgroud(),
        Container(
          margin: EdgeInsets.symmetric(
              horizontal: kDefaultPadding, vertical: kDefaultPadding / 2),
          child: Column(
            children: [
              ItemAccount(
                title: 'Chính sách quy định',
                icon: Icon(Icons.anchor_sharp),
              ),
              ItemAccount(
                ontap: () {
                  changePassword(context);
                },
                title: 'Đổi mật khẩu',
                icon: Icon(Icons.lock),
              ),
              ItemAccount(
                title: 'Đăng xuất',
                icon: Icon(Icons.login),
                ontap: () {
                  SingOutBtn(context);
                },
              ),
            ],
          ),
        ),
        Text('Version 0.1')
      ],
    );
  }

  Center buildCenter(BuildContext context) {
    return Center(
        child: ButtonCustom(
      color: kPrimaryColor,
      text: 'Login',
      colorSide: kPrimaryColor,
      colorText: kPrimaryColorText,
      press: () {
        Navigator.of(context).push(routeLogin());
      },
    ));
  }

  void changePassword(BuildContext context) {
    Navigator.of(context).push(routeChangePassword());
  }

  Future<void> SingOutBtn(BuildContext context) async {
    await authService.singOut();
    setState(() {
      Navigator.of(context).push(routeLogin());
    });
    Fluttertoast.showToast(
        msg: 'Đăng xuất thành công.', toastLength: Toast.LENGTH_SHORT);
  }
}
